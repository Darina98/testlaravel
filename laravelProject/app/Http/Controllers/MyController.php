<?php

namespace App\Http\Controllers;

use App\Models\activities;
use App\Models\alarms;
use App\Models\TypeAlarm;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentDate = Carbon::today();

        $activities = activities::whereDay("date", ">=", $currentDate)->orderBy("date", "asc")->take(5)->get();

        $alarms = alarms::whereDay("date", "<=", $currentDate)->orderBy("date", "desc")->take(5)->get();

        ///Saving back-up of activities/alarms
        $content = "activities: " . PHP_EOL;
        $content .= $activities->toJson();
        $content .= PHP_EOL . "alarms: " . PHP_EOL;
        $content .= $alarms->toJson();
        $zip = new \ZipArchive;
        $res = $zip->open(public_path() . DIRECTORY_SEPARATOR . Carbon::now()->year . ".zip", \ZipArchive::CREATE);
        if ($res === TRUE) {
            $zip->addFromString($currentDate . '.txt', $content);
            $zip->close();
        }

        return view('home', compact('activities', 'alarms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return "show invoked for " . $id;
    }

    public function showActivitiesDivers()
    {
        return view("activitiesDivers");
    }

    public function showAlarme($id)
    {
        $alarm = alarms::find($id);

        return view("alarme", compact("alarm"));
    }

    public function showActivitieSingle()
    {
        return view("activitieSingle");
    }

    public function showAlarmes()
    {
        $alarms = alarms::whereYear("date", "=", Carbon::today())->orderBy("date", "asc")->get();
        $currentMonth = Carbon::parse($alarms[0]->date)->format("M");

        $dateStart = Carbon::create(Carbon::now()->year, 1, 1, 0, 0, 0);
        $now = Carbon::now();
        $days = $dateStart->diffInDays($now);
        
        $alarmsCount= $alarms->count();
        
        $rateAlarms = $days/$alarmsCount;

        $typeAlarmsRates = alarms::select('typeAlarms.name', DB::raw(' COUNT(*) AS count '))
         ->leftJoin('typeAlarms', 'alarms.typeAlarms_id', '=', 'typeAlarms.id')
         ->groupBy('alarms.typeAlarms_id')
         ->get();

        return view("alarmes", compact("alarms", "currentMonth", "days", "alarmsCount", "rateAlarms", "typeAlarmsRates"));
    
    }

    public function showDocuments()
    {
        return view("documents");
    }

    public function showConnexion()
    {
        return view("connexion");
    }
    public function showFormation()
    {
        return view("formation");
    }

    public function showVehicles()
    {
        return view("vehicles");
    }

    public function showVehiclesChef()
    {
        return view("vehiclesChef");
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function testFilling()
    {

        $activity = new activities();
        $activity->description = "here must be description";
        $activity->featured_image = true;
        $activity->country = "France";
        $activity->city = "Paris";
        $activity->date = Carbon::now();
        $activity->save();

        $alarm = new alarms();
        $alarm->description = "here must be description";
        $alarm->featured_image = true;
        $alarm->country = "France";
        $alarm->city = "Paris";
        $alarm->commune = "commune";
        $alarm->date = Carbon::now();
        $alarm->save();

        $typeAlarm = new TypeAlarm();
        $typeAlarm->name = "fire";
        $typeAlarm->save();

        $typeAlarm->alarms()->save($alarm);      

    }
}
