<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAlarm extends Model
{
    use HasFactory;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'typeAlarms';

    protected $fillable = [
        "name"
    ];

    public function alarms() {
        return $this->hasMany(alarms::class, "typeAlarms_id", "id");
    }
}
