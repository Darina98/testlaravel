<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class alarms extends Model
{
    use HasFactory;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        "description",
        "country",
        "city",
        "date",
        "commune",
        "typeAlarms_id",
        "featured_image",
    ];
}
