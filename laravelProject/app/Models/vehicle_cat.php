<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vehicle_cat extends Model
{   use HasFactory;

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'vehicle_cat';

    protected $fillable = ["name"];

    public function up () 
    {
        return $this->hasMany(vehicles::class, "vehicle_cat_id", "id");
    }
}
