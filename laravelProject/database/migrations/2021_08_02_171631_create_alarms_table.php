<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alarms', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->text("description");
            $table->string("country", 12);
            $table->string("commune",12);
            $table->timestamp("date");
            $table->string("city");
            $table->integer("typeAlarms_id")->nullable();
            $table->boolean("featured_image");
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alarms');
    }
}
