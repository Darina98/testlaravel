@extends('layouts.common')

@section('content')
<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url('img/banner/2.jpg')"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>APR 1.3</h1>
						</div>
						<div class="point-desc">
							<div class="info-list info-active">
								<div class="row">
									<strong>Date:</strong>    
									<span>Jeudi 20 février 2020, 19:30 to 22:00</span>
								</div>
								<div class="row">
									<strong>Désignation:</strong>          
									<span>Exercice APR</span>
								</div>
								<div class="row">
									<strong>Entrée en service:</strong>                   
									<span>SDIS Chamberonne - Caserne 2</span>
								</div>
								<div class="row">
									<strong>Participants:</strong>                              
									<span>Selon tableau APR</span>
								</div>
								<div class="row">
									<strong>Directeur d’exercice:</strong>                              
									<span>Cap. C. Tille</span>
								</div>
								<div class="row">
									<strong>Commentaires:</strong>                              
									<span>Vacances scolaires</span>
								</div>
							</div>
							<a href="activities-drivers.php" class="btn">Toutes les activités</a>
						</div>
					</div>
					<aside class="aside">
						<div class="cont">
							<div class="info">
								<div class="title">
									<h4>Alarmes 2020</h4>
								</div>
								<div class="info-alarms">
									<div class="atten-info">
										<p>Nombre de jours :  041</p>
										<p>Moyenne annuelle :  27</p>
										<p>Une alarme tous les 13,7 jours</p>
									</div>
									<div class="row">
										<span>Feu</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Alarme automatique réelle </span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Alarme automatique</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Inondation</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Sauvetage / ascenseur</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>DCH</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Divers</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Officier de service</span>
										<span>0</span>
									</div>
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</section>
	</main>
</div>

@stop