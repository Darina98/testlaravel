@extends('layouts.common')

@section('content')
<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url('img/banner/1.jpg')"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>Activités & divers</h1>
						</div>
						<div class="block-tabs">
							<div class="links-tab cart-tabs">
								<a href="#cart-tabs_01" class="link btn active">Activités</a>
								<a href="#cart-tabs_02" class="link btn">Divers</a>
							</div>					
							<div class="table-list wrap-tabs">
								<div class="block-list cart-tabs__item cart-tabs__item--active" id="cart-tabs_01">
									<a href="activitie-single.php" class="row">
										<span class="number">003</span>
										<span class="month">Feu</span>
										<span class="desc">Ascenseur bloqué avec 1 personne</span>
										<span class="country">Ecublens</span>
										<span class="city">Mercredi</span>
										<span class="date">29.01</span>
										<span class="time">18:37</span>
										<i class="icon icon-picture"></i>
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>  
										<i class="icon icon-picture"></i>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>  
										<span class="time">18:37</span>  	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>   
										<span class="time">18:37</span> 	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>  
										<span class="time">18:37</span>  	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>  
										<span class="time">18:37</span>  	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>    	
										<span class="time">18:37</span>
									</a>
								</div>
								<div class="block-list cart-tabs__item" id="cart-tabs_02" style="display: none;">
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>
										<span class="month">Feu</span>
										<span class="desc">Ascenseur bloqué avec 1 personne</span>
										<span class="country">Ecublens</span>
										<span class="city">Mercredi</span>
										<span class="date">29.01</span>
										<span class="time">18:37</span>
										<i class="icon icon-picture"></i>
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">002</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>  
										<i class="icon icon-picture"></i>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>  
										<span class="time">18:37</span>  	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>   
										<span class="time">18:37</span> 	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>
										<span class="time">18:37</span>    	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span> 
										<span class="time">18:37</span>   	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>  
										<span class="time">18:37</span>  	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>  
										<span class="time">18:37</span>  	
									</a>
									<a href="activitie-single.php" class="row">
										<span class="number">001</span>	
										<span class="month">Feu</span>	                              
										<span class="desc">Défaut d’un poêle à pellets</span>                     
										<span class="country">Chavannes</span>         	
										<span class="city">Lundi</span>	          
										<span class="date">06.01</span>    	
										<span class="time">18:37</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<aside class="aside">
						<div class="cont">
							<div class="info">
								<div class="title">
									<h4>Alarmes 2020</h4>
								</div>
								<div class="info-alarms">
									<div class="atten-info">
										<p>Nombre de jours :  041</p>
										<p>Moyenne annuelle :  27</p>
										<p>Une alarme tous les 13,7 jours</p>
									</div>
									<div class="row">
										<span>Feu</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Alarme automatique réelle </span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Alarme automatique</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Inondation</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Sauvetage / ascenseur</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>DCH</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Divers</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Officier de service</span>
										<span>0</span>
									</div>
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</section>
	</main>
</div>
@stop
