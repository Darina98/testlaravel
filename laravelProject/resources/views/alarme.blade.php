@extends('layouts.common')

@section('content')
<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url({{ asset('img/banner/2.jpg')}}"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>{{$alarm->type_alarms_id}} {{$alarm->id}}</h1>
						</div>
						<div class="point-desc">
							<div class="info-list">
								<div class="row">
									<strong>Numéro d’alarme:</strong>    
									<span>{{$alarm->id}}</span>
								</div>
								<div class="row">
									<strong>Description:</strong>          
									<span>{{$alarm->description}}</span>
								</div>
								<div class="row">
									<strong>Commune:</strong>                   
									<span>{{$alarm->commune}}</span>
								</div>
								<div class="row">
									<strong>Date:</strong>                              
									<span>{{$alarm->date}}</span>
								</div>
								<div class="row">
									<strong>Type:</strong>                              
									<span>{{$alarm->type_alarms_id}}</span>
								</div>
							</div>
							<a href="\alarmes" class="btn">Toutes les alarmes</a>
						</div>
					</div>
					<aside class="aside">
						<div class="cont">
							<div class="info">
								<div class="title">
									<h4>Alarmes 2020</h4>
								</div>
								<div class="info-alarms">
									<div class="atten-info">
										<p>Nombre de jours :  041</p>
										<p>Moyenne annuelle :  27</p>
										<p>Une alarme tous les 13,7 jours</p>
									</div>
									<div class="row">
										<span>Feu</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Alarme automatique réelle </span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Alarme automatique</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Inondation</span>
										<span>1</span>
									</div>
									<div class="row">
										<span>Sauvetage / ascenseur</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>DCH</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Divers</span>
										<span>0</span>
									</div>
									<div class="row">
										<span>Officier de service</span>
										<span>0</span>
									</div>
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>
		</section>
	</main>
</div>
@stop