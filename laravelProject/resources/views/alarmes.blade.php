@extends('layouts.common')

@section('content')
<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url('img/banner/1.jpg')"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>Alarmes</h1>
						</div>
						<div class="table-list">
							<div class="title"><h2>{{$currentMonth}}</h2></div>
							<div class="block-list">
								@foreach ($alarms as $alarm)
									@php
								    	$month = Carbon\Carbon::parse($alarm->date)->format("M");
							 		@endphp
									@if($currentMonth !== $month)
										@php
											$currentMonth = $month;
										@endphp
								    	</div>
										<div class="title"><h2>{{$currentMonth}}</h2></div>
										<div class="block-list">
									@endif
									<a href="{{route('alarme', ['id' => $alarm->id])}}" class="row">
										<span class="number">{{$alarm->id}}</span>
										<span class="month">{{$currentMonth}}</span>
										<span class="desc">{{$alarm->description}}</span>
										<span class="country">{{$alarm->country}}</span>
										<span class="city">{{$alarm->city}}</span>
										<span class="date">{{Carbon\Carbon::parse($alarm->date)->format("m.d");}}</span>
										<span class="time">{{Carbon\Carbon::parse($alarm->date)->format("H:i");}}</span>
										@if ($alarm->featured_image)
											<i class="icon icon-picture"></i>
										@endif
									</a>
								@endforeach	
							</div>
						</div>
					</div>
					<aside class="aside">
						<div class="cont">
							<div class="info">
								<div class="title">
									<h4>Alarmes {{Carbon\Carbon::now()->year}}</h4>
								</div>
								<div class="info-alarms">
									<div class="atten-info">
										<p>Nombre de jours :  {{$days}}</p>
										<p>Moyenne annuelle :  {{$alarmsCount}}</p>
										<p>Une alarme tous les {{$rateAlarms}} jours</p>
									</div>
									@foreach ($typeAlarmsRates as $typeAlarmsRate)
										<div class="row">
											<span> {{$typeAlarmsRate->name}} :</span>
											<span> {{$typeAlarmsRate->count}} </span>
										</div>
									@endforeach
								</div>
							</div>
						</div>
						<div class="block-archives">
							<div class="title">
								<h4>Archives</h4>
							</div>
							<ul>
								<li><a href='{{asset(Carbon\Carbon::now()->year . ".zip")}}'>{{Carbon\Carbon::now()->year}}</a></li>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		</section>
	</main>
</div>

@stop