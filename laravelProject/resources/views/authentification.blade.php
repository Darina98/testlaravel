<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Authorization</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <div class="form-group">
        {{Form::open(array('route' => 'authentification', 'method' => 'post')) }}
        {{Form::label('user', 'User', ['class' => 'control-label'])}}
        {{Form::text('username', '', ['class' => 'form-control']) }}
        {{Form::label('pass', 'Password', ['class' => 'control-label'])}}
        {{Form::password('password', ['class' => 'form-control'])}}
        {{Form::submit('submit', ['class' => 'btn btn-primary'])}}
        {{Form::close()}}
    </div>
</body>