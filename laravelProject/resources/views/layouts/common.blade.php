<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex,nofollow">
        <meta name="theme-color" content="#fff"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!--         <link rel="shortcut icon" href="img/favicons/16.png"/>
        <link rel="apple-touch-icon" href="img/favicons/60.png"/>
        <link rel="apple-touch-icon" sizes="76x76" href="img/favicons/76.png"/>
        <link rel="apple-touch-icon" sizes="120x120" href="img/favicons/120.png"/>
        <link rel="apple-touch-icon" sizes="152x152" href="img/favicons/152.png"/> -->
        <title>Sdis</title>
        <meta name="description" content="">
        <meta property="og:title" content="Sdis"/>
		<meta property="og:description" content=""/>
<!-- 		<meta property="og:image" content="img/favicons/opengraph.png"/> -->
        <!--[if IE]>
            <script>
                document.createElement('header');
                document.createElement('nav');
                document.createElement('main');
                document.createElement('section');
                document.createElement('article');
                document.createElement('aside');
                document.createElement('footer');
            </script>
        <![endif]-->

        <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.css" rel="stylesheet">
        <link rel="stylesheet" href='{{ asset("css/style.css")}}'>
    </head>
    <body>
        <header class="header">
            <div class="info">
                <div class="wrap">
                    <a href="mailto:contact@sdis-chamberonne.ch">contact@sdis-chamberonne.ch</a>
                </div>
            </div>
            <div class="wrap">
                <div class="flex">
                    <a href="" class="logo">
                        <img src="{{ asset('img/logo-main.svg')}}" alt="">
                    </a>
                    <div class="action">
                        <nav class="nav">
                            <ul class="menu">
                                <li class="item">
                                    <a href="" class="link">Présentation</a>
                                    <ul>
                                        <li><a href="formation.php">Sites</a></li>
                                        <li><a href="#">Organisation</a></li>
                                        <li><a href="formation.php">Missions</a></li>
                                        <li><a href="formation.php">Formation</a></li>
                                        <li><a href="{{route('vehicles')}}">Véhicules</a></li>                
                                    </ul>
                                </li>
                                <li class="item">
                                    <a href="{{ route('alarmes') }}" class="link">Alarmes</a>
                                </li>
                                <li class="item">
                                    <a href="{{ route('activitites_divers') }}" class="link">Activités & divers</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="link">Contact</a>
                                </li>
                            </ul>
                        </nav>
                        <a href="{{ route('connexion') }}" class="connect">
                            <i class="icon icon-profile"></i>
                            <span>Connexion</span>
                        </a>
                        <div class="hamburger">
                            <span class="line"></span> 
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="shadow"></div>
        <div class="m-panel">
            <div class="content">
                <span class="close icon-signs"></span>
                <nav class="nav">
                    <ul class="menu">
                        <li class="item">
                            <a href="" class="link">Présentation</a>
                            <ul>
                                <li><a href="formation.php">Sites</a></li>
                                <li><a href="#">Organisation</a></li>
                                <li><a href="formation.php">Missions</a></li>
                                <li><a href="formation.php">Formation</a></li>
                                <li><a href="{{ route('vehicles') }}"">Véhicules</a></li>                
                            </ul>
                        </li>
                        <li class="item">
                            <a href="{{ route('alarmes') }}"" class="link">Alarmes</a>
                        </li>
                        <li class="item">
                            <a href="{{ route('activitites_divers') }}"" class="link">Activités & divers</a>
                        </li>
                        <li class="item">
                            <a href="#" class="link">Contact</a>
                        </li>
                    </ul>
                </nav>
                <a href="{{ route('connexion') }}"" class="connect">
                    <i class="icon icon-profile"></i>
                    <span>Connexion</span>
                </a>
            </div>
        </div>

        @yield("content")

        <footer class="footer">
            <div class="wrap">
                <div class="flex">
                    <a href="" class="logo">
                        <img src='{{ asset("img/logo-foot.svg")}}' alt="" class="white">
                    </a>
                    <div class="footer-info">
                        <strong>SDIS Chamberonne</strong>
                        <span>Case postale 346</span> 
                        <span>1024 Ecublens</span>
                    </div>
                    <div class="copyright">
                        <p>© 2020 SDIS Chamberonne | Créé par Emblematik</p>
                    </div>
                </div>
            </div>
        </footer>
        
        <div class="modal modal-contacts" id="modal-contacts" style="display: none;">
            <div class="content-modal  modal-contacts-content" id="modal-contacts-wrap" style="display: none;">
                <span class="close"><img src="{{ asset('img/icons/close.svg')}}" alt=""></span>
                <form action="">
                    <div class="title">
                        <h4>Contacts</h4>
                    </div>
                    <div class="row">
                        <div class="input">
                            <input type="text" placeholder="Your name">
                        </div>
                        <div class="input">
                            <input type="text" placeholder="Your message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input">
                            <input type="text" placeholder="Your mail">
                        </div>
                        <div class="block-btn">
                            <button type="" class="fom-button">Send message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
        <!-- CDN -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.js"></script>
        
        <!-- LOCAL -->
        <script src="{{ asset('js/function_slider.js')}}"></script>
        <script src="{{ asset('js/main.js')}}"></script>
    </body>
    </html>

    </div>

