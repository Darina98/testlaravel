@extends('layouts.common')

@section('content')

<div class="wrapper">
	<main>
		<div class="banner" style="background-image: url('img/banner/1.jpg')"></div>
		<section class="container vehicles-section">
			<div class="wrap">
				<section class="section-text single-text">
					<div class="title">
						<h1>Les Véhicules du SDIS</h1>
					</div>
					<div class="editor">
						<p>Le parc du SDIS Chamberonne comprend 9 véhicules et plusieurs engins : 2 véhicules légers « officier de service », 1 tonne-pompe 2000l dernière génération, 1 tonne-pompe 1800l, 1 véhicule lourd d’accompagnement, 2 véhicules mi-lourds d’accompagnement et de transport de personnes, 2 véhicules légers de type jeep, 2 moto-pompes, 1 remorque « canon », 1 remorque « protection respiratoire », 2 remorques « transport de tuyaux », 1 remorque « inondation » et 2 remorques diverses.</p>
					</div>
					<div class="action-btn flex">
						<a href="#" class="btn">All</a>
						<a href="#" class="btn">Caserne 1</a>
						<a href="#" class="btn">Caserne 2</a>
					</div>
				</section>
			</div>
		</section>
		<section class="vehicles-posts">
			<div class="wrap">
				<div class="block-posts flex">
					<a href="vehicles-chef.php" class="item-posts">
						<div class="img" style="background-image: url('img/content/1.jpg')"></div>
						<span class="title-post">Chef d’intervention (VCI)</span>
						<span class="desc">Chambro102</span>
					</a>
					<a href="vehicles-chef.php" class="item-posts">
						<div class="img" style="background-image: url('img/content/1.jpg')"></div>
						<span class="title-post">Chef d’intervention (VCI)</span>
						<span class="desc">Chambro102</span>
					</a>
					<a href="vehicles-chef.php" class="item-posts">
						<div class="img" style="background-image: url('img/content/1.jpg')"></div>
						<span class="title-post">Chef d’intervention (VCI)</span>
						<span class="desc">Chambro102</span>
					</a>
					<a href="vehicles-chef.php" class="item-posts">
						<div class="img" style="background-image: url('img/content/1.jpg')"></div>
						<span class="title-post">Chef d’intervention (VCI)</span>
						<span class="desc">Chambro102</span>
					</a>
					<a href="vehicles-chef.php" class="item-posts">
						<div class="img" style="background-image: url('img/content/1.jpg')"></div>
						<span class="title-post">Chef d’intervention (VCI)</span>
						<span class="desc">Chambro102</span>
					</a>
				</div>
			</div>
		</section>
	</main>
</div>

@stop