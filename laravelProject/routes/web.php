<?php

use App\Models\alarms;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MyController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/// testFilling db with dummy data
Route::get('/testFilling', [MyController::class, 'testFilling']);

Route::get('/', [MyController::class, 'index'])->name('home');

Route::get('/adminPanel', [AdminController::class, 'index']);

Route::get('/activitites_divers', [MyController::class, 'showActivitiesDivers'])->name('activitites_divers');;

Route::get('/alarme/{id}', [MyController::class, 'showAlarme'])->name('alarme');

Route::get('/activitie_single', [MyController::class, 'showActivitieSingle'])->name('activitie_single');

Route::get('/alarmes', [MyController::class, 'showAlarmes'])->name('alarmes');

Route::get('/vehicles', [MyController::class, 'showVehicles'])->name('vehicles');

Route::get('/documents', [MyController::class, 'showDocuments'])->name('documents');

Route::get('/connexion', [MyController::class, 'showConnexion'])->name('connexion');

Route::get('/formation', [MyController::class, 'showFormation'])->name('formation');

Route::get('/vehicles', [MyController::class, 'showVehicles'])->name('vehicles');

Route::get('/vehicles_chef', [MyController::class, 'showVehiclesChef'])->name('vehiclesChef');

Auth::routes();